*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Variables    ForgotPwAssertion.py
Variables    ForgotPwLocators.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
*** Variables ***
*** Keywords ***
Click Back To Lgn
    [Documentation]  "Clicks the 'back to login' link when on the forgotton password page"
    wait until element is visible  ${BACK_TO_LOGIN}
    element should contain    ${FORGOT_CONTENT_CHECK}    ${FORGOT_CONTENT_TEXT}
    Run Keyword until Success    click element  ${BACK_TO_LOGIN}