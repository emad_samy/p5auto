*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Resource    ../../Resources/SharedPages/BannerKeyword.robot
Variables     ../../Resources/CommonKeywords/CommonVariables.py
Variables     ../../Resources/CommonKeywords/CommonLocators.py
Variables     ../../Resources/CommonKeywords/CommonAssertion.py
*** Variables ***
&{URL}=     QA=${QAURL}   PROD=${PRODURL}
${RETRY}                10      #could be  either as timeout or count
${RETRY_INTERVAL}       1s      #time to wait before trying to run the keyword
*** Keywords ***
start web test
    [Documentation]    "Launches browser, maximizes the window, and sets wait times. Args: browser is set to chrome by default."
    [Arguments]    ${browser}= ${BROWSER}
    open browser    about:blank    ${browser}
    maximize browser window
    setTimes
setTimes
    [Documentation]  "Sets the selenium waittime and timout. Args: imp_wait_time is set to 20s by default, sel_timeout is set to 10s by default"
    [Arguments]  ${imp_wait_time}=${IMPLICIT_WAIT_TIME}   ${sel_timeout}=${SELENIUM_TIMEOUT}
    set selenium implicit wait     ${imp_wait_time}
    set selenium timeout    ${sel_timeout}
openWindow
    [Documentation]  "Launches browser window. Args: Browser set to chrome by default"
    [Arguments]  ${url}=${URL.${ENVIRONMENT}}   ${browser}=${BROWSER}
    open browser    ${url}    ${browser}
    maximize browser window

open login url
    [Documentation]    "Uses start web test then changes to silicon expert login time."
    [Arguments]    ${url}=${URL.${ENVIRONMENT}}     ${browser}=${BROWSER}
    start web test    ${browser}
    go to    ${url}
open link
    [Documentation]    "Go to a desired url."
    [Arguments]    ${link}
    go to    ${link}
logout action
    [Documentation]  "All Tests logout and close window on successful execution"
    logout
pause
    [Documentation]  "Pauses the robot for a brief period in some loading situations. Args: pause_time is set to 2s by default."
    [Arguments]  ${pause_time}=${SLEEP_TIME}
    sleep  ${pause_time}

Run Keyword until Success
    [Documentation]     this keyword will help to make your test scuceeded and if failed it retry to make it success this could be used for clcik element/ button and input text
    [Arguments]  ${KW}   @{KWARGS}   ${retry}=${RETRY}    ${retry_interval}=${RETRY_INTERVAL}
    Wait Until Keyword Succeeds    ${retry}    ${retry_interval}    ${KW}     @{KWARGS}

