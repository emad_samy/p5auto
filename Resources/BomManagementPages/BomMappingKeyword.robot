*** Settings ***
Library  SeleniumLibrary
Variables     ../../Resources/BomManagementPages/BomMappingAssertion.py
Variables     ../../Resources/BomManagementPages/BomMappingLocators.py
*** Variables ***

*** Keywords ***
input check
    ${text}=    get text    ${SET_COL_1}
    ${text2}=    get text    ${SET_COL_2}
    IF   "${text}" != "${MPN_TEXT}"
        change col 1
        END
    IF   "${text2}" != "${MANU_TEXT}"
        change col 2
        END

change col 1
    wait until element is visible    ${SET_COL_1}
    Run Keyword until Success    click element    ${SET_COL_1}
    wait until element is visible    ${MPN}
    Run Keyword until Success    click element    ${MPN}
change col 2
    wait until element is visible    ${SET_COL_2}
    Run Keyword until Success    click element    ${SET_COL_2}
    wait until element is visible    ${MANUFACTURER}
    Run Keyword until Success    click element    ${MANUFACTURER}
click continue
    wait until element is visible    ${CONTINUE}
    Run Keyword until Success    click element    ${CONTINUE}
progress through bom mapping
    input check
    click continue



