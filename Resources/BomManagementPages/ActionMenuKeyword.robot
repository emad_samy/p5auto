*** Settings ***
Library  SeleniumLibrary
Variables     ../../Resources/BomManagementPages/ActionMenuLocators.py
*** Variables ***

*** Keywords ***
delete bom
    wait until element is visible    ${DELETE}
    Run Keyword until Success    click element    ${DELETE}
check delete
    wait until element is visible    ${DELETE_BOX}
    Run Keyword until Success    click element    ${DELETE_BOX}
click del button
    wait until element is visible    ${DELETE_TEXT}
    Run Keyword until Success    click element    ${DELETE_TEXT}
deletion sequence
    delete bom
    check delete
    click del button




