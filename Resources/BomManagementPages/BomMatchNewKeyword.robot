*** Settings ***
Library  SeleniumLibrary
Variables     ../../Resources/BomManagementPages/BomMatchNewAssertion.py
Variables     ../../Resources/BomManagementPages/BomMatchNewLocators.py
*** Variables ***
*** Keywords ***
verify new match page
    wait until element is visible    ${NEW_MATCH_CONTENT}
progress through new bom match
    verify new match page