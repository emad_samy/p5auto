*** Settings ***
Library  SeleniumLibrary
Variables     ../../Resources/BomManagementPages/BomMatchAssertion.py
Variables     ../../Resources/BomManagementPages/BomMatchLocators.py
*** Variables ***
*** Keywords ***
verify match page
    wait until element is visible    ${MATCH_HEADING}
    element should contain    ${MATCH_HEADING}      ${MATCH_HEADING_TEXT}
click finish
    wait until element is visible    ${MATCH_FINISH}
    Run Keyword until Success    click element    ${MATCH_FINISH}
progress through bom match
    verify match page
    click finish


