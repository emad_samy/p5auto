*** Settings ***
Library  SeleniumLibrary
Variables     ../../Resources/BomManagementPages/MyBomsAssertion.py
Variables     ../../Resources/BomManagementPages/MyBomsLocators.py
*** Variables ***
*** Keywords ***
Verify MyBoms Page
    wait until element is visible    ${MY_BOMS_HEADER}
    element should contain    ${MY_BOMS_HEADER}     ${MY_BOMS_HEADER_CONTENT}
click project
    [Arguments]    ${project}=${AUTOMATION_PROJECT}
    wait until element is visible    ${project}
    Run Keyword until Success    click element    ${project}
click archive
    #this page has another tab, dont have to do this till e2e works
progress through my boms
    [Arguments]    ${chosen_project}=${AUTOMATION_PROJECT}
    Verify MyBoms Page
    click project    ${chosen_project}


