#fix variable name
*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/ForgotPwPage/ForgotPwKeyword.robot
Resource   ../../Resources/CommonKeywords/CommonKeyword.robot
Resource   ../../Resources/TroubleWithAccountPage/TroubleWithAccountKeyword.robot
Variables     LoginLocators.py
Variables     LoginAssertion.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
Documentation  "These keywords are useful on the login page before access to the platform is granted."

*** Variables ***
&{PW}    QA=${QAPW}    PROD=${PRODPW}

*** Keywords ***
login
    [Documentation]  "This case simulates User logging in, as well as sets the selenium wait times, and enters the platform"
    [Arguments]    ${name}=${NAME}   ${pw}=${PW.${ENVIRONMENT}}
    writeUsername   ${name}
    writePassword   ${pw}
    clickRememberMe
    accessPlatform
    #log to console    %{EXAMPLE}
    handle logged in account
forgotPassword
    [Documentation]  "The case for when a user forgets their password"
    setTimes
    openWindow
    writeUsername
    clickForgotPw
    clickBackToLgn
troubleShoot
    [Documentation]  "When a user has trouble accessing their account"
    setTimes
    openWindow
    clickTrblsht
    clickPrev
    clickTrblsht
    clickContactUs
login action
    [Documentation]  "All Tests login prior to unique events"
     [Arguments]    ${name}=${NAME}   ${pw}=${PW.${ENVIRONMENT}}
    login    ${name}   ${pw}
writeUsername
    [Documentation]  "Inputs username. Args: Name set to liam.calnan@siliconexpert.com as default"
    [Arguments]  ${name}=${NAME}
    wait until element is visible  ${USERNAME}
    Run Keyword until Success    input text  ${USR_ID}   ${name}
writePassword
    [Documentation]  "User password is entered. Args: Pw is set to liam's password as default"
    [Arguments]  ${pw}=${PW.${ENVIRONMENT}}
    Run Keyword until Success    input text  ${PW_ID}    ${pw}
accessPlatform
    [Documentation]  "Clicks the button to login to the website"
    click element  ${LGN_BTN}
access silicon expert
    [Arguments]      ${name}=${NAME}      ${pw}=${PW.${ENVIRONMENT}}       ${url}=${URL.${ENVIRONMENT}}      ${browser}=${BROWSER}
    [Documentation]    "Logs user in"
    open login url  ${url}   ${browser}
    login action  ${name}   ${pw}
handle logged in account
    [Documentation]    "Proceeds to website even if user is logged in elsewhere. Temporary change in implicit wait time to avoid needless delay."
    ${pageTitle}=   get window titles
    #log to console    ${pageTitle}
    #SiliconExpert | Customer Login

#    ${alreadyLoggedIn} =   Get Element Count   ${LOGGED_IN_LINK}
    IF   "${pageTitle}" == "${LOGGED_IN_FAIL_TITLE}"
        logged in link click
        END
logged in link click
    [Documentation]    "Clicks the proceed link that appears when user is already logged in elsewhere."
    wait until element is visible    ${LOGGED_IN_LINK}
    click element    ${LOGGED_IN_LINK}
clickRememberMe
    [Documentation]  "Clicks remember me checkbox"
    wait until element is visible  ${REMEMBERME}
    click element   ${REMEMBERME}
clickForgotPw
    [Documentation]  "Clicks link to 'forgot password'"
    wait until element is visible  ${FORGOT_PW}
    click element  ${FORGOT_PW}
clickTrblsht
    [Documentation]  "Clicks link to 'having trouble accessing account'"
    wait until element is visible  ${HAVING_TROUBLE}
    click element  ${HAVING_TROUBLE}
clickContactUs
    [Documentation]  "Clicks the contact us link"
    wait until element is visible  ${CONTACT_US}
    click element  ${CONTACT_US}
login content check
    element should contain    ${LOGIN_CONTENT_CHECK}    ${LOGIN_CONTENT_TEXT}