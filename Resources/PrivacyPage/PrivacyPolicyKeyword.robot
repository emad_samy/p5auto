*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Variables     PrivacyLocators.py
Variables     PrivacyAssertion.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
*** Variables ***
*** Keywords ***
handle privacy
    [Documentation]  "Goes back to SE main page"
    switch window    new
    pause
    wait until element is visible   ${PRIVACY_CONTENT}
    element should contain    ${PRIVACY_CONTENT_CHECK}    ${PRIVACY_CONTENT_TEXT}
    switch window  main