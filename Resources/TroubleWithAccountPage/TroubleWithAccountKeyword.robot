*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Variables     TroubleLocators.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
*** Variables ***
*** Keywords ***
clickPrev
    [Documentation]  "Clicks the 'previous page' link when on the having trouble page"
    wait until element is visible  ${BACK_TO_PREV}
    Run Keyword until Success    click element  ${BACK_TO_PREV}