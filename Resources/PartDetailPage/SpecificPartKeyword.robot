*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Variables     PartDetailLocators.py
Variables     PartDetailAssertion.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
*** Variables ***
*** Keywords ***
Verify Part Page loaded sucss
    [Documentation]    "Waits till the part page loads then switches back to main."
    switch window    new
    pause
    wait until element is visible  ${PART_CONTENT}
    element text should be    ${PART_TEXT_CHECK}    ${PART_TEXT_TEXT}
    switch window    main

