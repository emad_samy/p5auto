*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Variables     AlertManagerLocators.py
Variables     AlertManagerAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
*** Variables ***
*** Keywords ***
Verify Alert Manager Page loaded sucss
    [Documentation]    "Verify the page is loaded."
    Run Keyword until Success    wait until element is visible    ${ALERT_CONTENT}
    element text should be    ${ALERT_CONTENT}      ${TIMELINE}