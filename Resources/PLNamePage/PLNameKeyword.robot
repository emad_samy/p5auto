*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Variables     PLNameLocators.py
Variables     PLNameAssertion.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
*** Variables ***
*** Keywords ***
Verify PL Page loaded sucss
    [Documentation]    "Waits for PL page to load then switches back to the main window."
    switch window    new
    pause
    wait until element is visible  ${PL_CONTENT}
    element text should be    ${PL_CONTENT_CHECK}   ${PL_CONTENT_TEXT}
    switch window    main