*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Variables     ManufacturerLocators.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
*** Variables ***
*** Keywords ***
Verify Manu Page loaded sucss
    [Documentation]    "Verify the page is loaded."
    switch window    new
    pause
    wait until element is visible  ${MANU_CONTENT}
    switch window    main