############################################################
# georisk variables
GEO_CSV_FILE= "C:/Users/Liam/Desktop/Intern/p5auto/TestData/georisk.csv"
GEO_TAB_BTN = "id:mat-tab-label-0-0"
FABRICATION_SITE_TAB = "id:mat-tab-label-1-0"
WAFER_TEST_TAB = "id:mat-tab-label-1-1"
ASSEMBLY_SITE_TAB = "id:mat-tab-label-1-2"
FINAL_TEST_TAB = "id:mat-tab-label-1-3"
LOAD_CHECK = "xpath=//mat-tab-body/div[1]/app-geo-parent[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/mat-card[1]/lib-manuf-site-status-graph[1]/div[1]/div[1]/h5[1]"
MAP_CONTENT = 'xpath=//mat-tab-body/div[1]/app-geo-parent[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/mat-card[1]/mat-tab-group[1]/div[1]'
COUNTRY_OF_ORIGIN_TAB = "id:mat-tab-label-1-4"
SELECT_EVENT_TYPE = 'xpath=//mat-tab-body/div[1]/app-geo-parent[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/mat-card[1]/lib-covid-impacted-parts-status-graph[1]/div[1]/div[1]/mat-form-field[1]/div[1]/div[1]/div[1]'
DISEASES = "xpath=//span[contains(text(),'diseases')]"
DROUGHTS = "xpath=//span[contains(text(),'droughts')]"
EARTHQUAKES = "xpath=//span[contains(text(),'earthquakes')]"
FACTORY_FIRES = "xpath=//span[contains(text(),'factory fires')]"
POLITICAL_SITUATIONS = "xpath=//span[contains(text(),'political situations')]"
PORT_DISRUPTIONS = "xpath=//span[contains(text(),'ports disruptions')]"
POWER_OUTAGES = "xpath=//span[contains(text(),'power outages')]"
UNKNOWN = "xpath=//span[contains(text(),'unknown')]"
DISEASE1 = "xpath=//span[contains(text(),'covid-19')]"
DISEASE2 = "xpath=//span[contains(text(),'covid-19 impact taiwan supply chain june 2021')]"
DISEASE3 = "xpath=//span[contains(text(),'greatek electronics production impact due to covid cluster june 2021')]"
DISEASE4 = "xpath=//span[contains(text(),'king yuan electronics production halt due to covid cluster june 2021')]"
DISEASE5 = "xpath=//span[contains(text(),'malaysia covid-19 lockdown june 2021')]"
DROUGHT2 = "xpath=//span[contains(text(),'taiwan drought feb 2021')]"
EARTHQUAKE1 = "xpath=//span[contains(text(),'2020 yilan earthquake')]"
EARTHQUAKE2 = "xpath=//span[contains(text(),'japan northeastern region earthquake feb 13, 2021')]"
FIRE1 = "xpath=//span[contains(text(),'13-jan-2021 walsin dongguan mlcc china plant fire')]"
FIRE2 = "xpath=//span[contains(text(),'naka factory fire 19 mar, 2021')]"
FIRE3 = "xpath=//span[contains(text(),' oct 2020 akm japan fire ')]"
POLITICAL1 = "xpath=//span[contains(text(),' us dod banned list ')]"
PORT1 = "xpath=//span[contains(text(),' suez canal shipping stuck mar 23, 2021 ')]"
POWER1 = "xpath=//span[contains(text(),' austin, texas power outage feb 16, 2021 ')]"
POWER2 = "xpath=//span[contains(text(),' taiwan power outage may 13, 2021 ')]"
POWER3 = "xpath=//span[contains(text(),' umc power outage jan 09, 2021 ')]"
POWER4 = "xpath=//span[contains(text(),' tsmc power outage 14 apr, 2021 ')]"
SELECT_EVENT_NAME = 'xpath=//mat-tab-body/div[1]/app-geo-parent[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/mat-card[1]/lib-covid-impacted-parts-status-graph[1]/div[1]/div[2]/mat-form-field[1]/div[1]/div[1]'
NUM_PARTS_BTN = "id:mat-button-toggle-4-button"
ASCENDING_BTN = "id:mat-button-toggle-6-button"
DESCENDING_BTN = "id:mat-button-toggle-7-button"
HIGHRISK_BTN = "id:mat-button-toggle-5-button"
DROPDOWN_CONTENT = "xpath = //mat-tab-body/div[1]/app-geo-parent[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/mat-card[1]/lib-covid-impacted-parts-status-graph[1]/div[2]/h5[1]"
PCB_CONTENT = 'xpath=//mat-tab-body/div[1]/app-geo-parent[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/mat-card[1]/lib-geo-risk-by-product[1]/div[1]/div[2]'
MR_CONTENT = 'xpath=//mat-tab-body/div[1]/app-geo-parent[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[2]/mat-card[1]/lib-manuf-geo-risk-grade-graph[1]/div[1]/div[2]'
MR_NUM_PARTS_BTN = "id:mat-button-toggle-8-button"
MR_ASCENDING = "id:mat-button-toggle-10-button"
MR_DESCENDING = "id:mat-button-toggle-11-button"
MR_HIGHRISKBTN = "id:mat-button-toggle-9-button"
EDITABLE_RISK = "xpath=//span[contains(text(),'Editable Risks')]"
FAB_SITE_PERCENT = "xpath=//input[@id='mat-input-0']"
WAFER_TEST_PERCENT = "xpath=//input[@id='mat-input-1']"
ASSEMBLY_SITE_PERCENT = "xpath=//input[@id='mat-input-2']"
FINAL_TEST_PERCENT = "xpath=//input[@id='mat-input-3']"
FAB_SITE_PERCENT2 = "xpath=//input[@id='mat-input-4']"
WAFER_TEST_PERCENT2 = "xpath=//input[@id='mat-input-5']"
ASSEMBLY_SITE_PERCENT2 = "xpath=//input[@id='mat-input-6']"
FINAL_TEST_PERCENT2 = "xpath=//input[@id='mat-input-7']"
RESET_PERCENT = "xpath=//span[contains(text(),'RESET')]"
SAVE_BTN = "xpath=//span[contains(text(),'SAVE')]"
RESET_FILTER = "xpath=//span[contains(text(),'Reset Filter')]"
EDITABLE_CLOSE = "xpath=//mat-icon[contains(text(),'close')]"
SHOW_PARTS = "xpath=//span[contains(text(),'Show Parts')]"
SHOW_PARTS_BACK = "xpath=//span[contains(text(),'Back')]"
SEARCH_BOX_PARTS = "xpath=/html[1]/body[1]/app-root[1]/span[1]/page-template[1]/main[1]/div[1]/div[1]/div[1]/article[1]/div[1]/app-parts-list-table[1]/div[3]/mat-card[1]/div[2]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]"
SEARCH_PART = "xpath=//input[@id='mat-input-0']"
PART_SEARCH_RESET = "xpath=//span[contains(text(),'Reset Search')]"
VIEW_MAP = "xpath=//body/app-root[1]/span[1]/page-template[1]/main[1]/div[1]/div[1]/div[1]/article[1]/div[1]/app-parts-list-table[1]/div[3]/mat-card[1]/div[3]/div[1]/mat-dialog-content[1]/mat-table[1]/mat-row[1]/mat-cell[1]/a[1]"
QCA9882 = "xpath=//div[contains(text(),'QCA9882-BR4A-R')]"
QUALCOMM = "xpath=//div[contains(text(),'QUALCOMM')]"
WIRELESS_LAN = "xpath=//div[contains(text(),'802.11 Wireless LAN')]"
SELECT_BOM = "xpath=//span[contains(text(),'Select BOM')]"
SELECT_BOM_SEARCH_BOX = "xpath=/html[1]/body[1]/div[3]/div[2]/div[1]/mat-dialog-container[1]/app-select-bom-modal[1]/div[1]/div[2]/div[1]/input[1]"
SELECT_BOM_TEST = "xpath=//body/div[3]/div[2]/div[1]/mat-dialog-container[1]/app-select-bom-modal[1]/div[3]/div[2]/div[1]/div[1]/div[1]"
SELECT_BOM_CLOSE = "xpath=//body/div[3]/div[2]/div[1]/mat-dialog-container[1]/app-select-bom-modal[1]/div[4]/div[1]/button[1]/span[1]"
SELECT_BOM_RESET = "xpath=//body/div[3]/div[2]/div[1]/mat-dialog-container[1]/app-select-bom-modal[1]/div[1]/div[2]/div[3]/button[1]/span[1]"
SELECT_BOM_SEARCH_BTN = "xpath=//span[contains(text(),'Search')]"
SELECT_BOM_CONTENT = "xpath=//mat-tab-body/div[1]/app-geo-parent[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/mat-card[1]/lib-covid-impacted-parts-status-graph[1]/div[2]/h5[1]"
############################################################
# market forecast variables
MARKET_TAB_BTN = "id:mat-tab-label-0-1"
MARKET_CSV_FILE= "C:/Users/Liam/Desktop/Intern/p5auto/TestData/marketSearch.csv"
SEARCH_BOX = "id:mat-input-3"
VIEW_ALL_BTN = "id:mat-button-toggle-23-button"
SOLVED_BTN = "id:mat-button-toggle-24-button"
UNSOLVED_BTN = "id:mat-button-toggle-25-button"
MPN_CONTENT = 'xpath=//mat-tab-body/div[1]/app-forecast-parent[1]/div[2]/div[1]/div[1]/div[1]/div[1]/app-part-grid-table[1]/mat-card[1]/div[3]/mat-table[1]'
APPLY_BTN = 'xpath = //*[@id="mat-tab-content-0-1"]/div/app-forecast-parent/div[2]/div/div/div[1]/div[1]/app-part-grid-table/mat-card/div[1]/div[4]/div/button'
# insights
DATE_BTN = "//button[@id='mat-button-toggle-26-button']"
PRICE_BTN = "//button[@id='mat-button-toggle-27-button']"
STOCK_BTN = "xpath=//button[@id='mat-button-toggle-28-button']"
LEAD_TIME_BTN = "xpath=//button[@id='mat-button-toggle-29-button']"
FILTER_BTN = "xpath=//app-header/div[1]/div[2]/div[1]/div[1]/div[1]/button[1]/span[1]"
FILTER_RESET = "xpath=//span[contains(text(),'Reset Filters')]"
MANU_SEARCH = "xpath=/html[1]/body[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[2]/div[2]/form[1]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]"
THREE_M = "xpath=//span[contains(text(),'3M')]"
JUNE_1 = "xpath=//tbody/tr[1]/td[2]/div[1]"
JUNE_30="xpath=//tbody/tr[5]/td[6]/div[1]"
APPLY_FILTER="xpath=//span[contains(text(),'APPLY')]"
CANCEL_FILTER="xpath=//body/div[3]/div[2]/div[1]/div[1]/div[1]/div[8]/button[1]/span[1]"
FROM_DATE = "xpath=//body/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/mat-datepicker-toggle[1]/button[1]/span[1]/*[1]"
TO_DATE = "xpath=//body/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[2]/div[2]/mat-datepicker-toggle[1]/button[1]/span[1]/*[1]"
PRICE_INC = "xpath=//body/div[3]/div[2]/div[1]/div[1]/div[1]/div[4]/div[2]/mat-button-toggle-group[1]/mat-button-toggle[1]/button[1]/div[1]"
PRICE_DEC = "xpath=//body/div[3]/div[2]/div[1]/div[1]/div[1]/div[4]/div[2]/mat-button-toggle-group[1]/mat-button-toggle[2]/button[1]/div[1]"
INV_INC = "xpath=//body/div[3]/div[2]/div[1]/div[1]/div[1]/div[5]/div[2]/mat-button-toggle-group[1]/mat-button-toggle[1]/button[1]/div[1]"
INV_DEC = "xpath=//body/div[3]/div[2]/div[1]/div[1]/div[1]/div[5]/div[2]/mat-button-toggle-group[1]/mat-button-toggle[2]/button[1]/div[1]"
LEAD_INC ="xpath=//body/div[3]/div[2]/div[1]/div[1]/div[1]/div[6]/div[2]/mat-button-toggle-group[1]/mat-button-toggle[1]/button[1]/div[1]"
LEAD_DEC ="xpath=//body/div[3]/div[2]/div[1]/div[1]/div[1]/div[6]/div[2]/mat-button-toggle-group[1]/mat-button-toggle[2]/button[1]/div[1]"
############################################################
# sub tier supply variables
STS_TAB_BTN = "id:mat-tab-label-0-2"
STS_SEARCH_BOX = "id:mat-input-0"
RESET_BTN = 'xpath = //*[@id="mat-tab-content-0-2"]/div/app-sub-tier-parent/div[2]/div[1]/mat-card/app-sub-tier-grid/div/mat-card/div[1]/div[3]/div[3]'
MINERAL_MENU = 'xpath=//mat-tab-body/div[1]/app-sub-tier-parent[1]/div[2]/div[2]/mat-card[1]/app-sub-tier-precious-minerals-graph[1]/div[1]/div[2]/mat-form-field[1]/div[1]/div[1]/div[1]'
GOLD = "xpath=//span[contains(text(),'GOLD (USD per Troy Ounce):')]"
ANY_MINERAL = "xpath=//span[contains(text(),'GOLD (USD per Troy Ounce):')]"
MINERAL_CONTENT = 'xpath=//mat-tab-body/div[1]/app-sub-tier-parent[1]/div[2]/div[2]/mat-card[1]/app-sub-tier-precious-minerals-graph[1]/div[3]/canvas[1]'
DAILY_BTN = "id:mat-button-toggle-13-button"
WEEKLY_BTN = "id:mat-button-toggle-14-button"
MONTHLY_BTN = "id:mat-button-toggle-15-button"
STS_FOOTER = "id:popoverID"
VISHAY = "vishay"
############################################################
# technological profile variables
TECH_PROF_TAB_BTN = "id:mat-tab-label-0-3"
TECH_SEARCH_BOX = 'xpath=//*[@id="mat-input-0"]'
TECH_FOOTER = 'xpath = //*[@id="popoverID"]/div/span'
MORE_ACTIONS = 'xpath= //*[@id="mat-tab-content-0-3"]/div/app-tech-parent/div[3]/div[1]/div[2]/card/div[2]/div/chart/charts-toggle-menu/action-menu/div[2]'
DONUT_CHART = 'xpath = //*[@id="parent-menu"]/ul/menu-item[2]/li[1]'
PIE_CHART = 'xpath= //*[@id="parent-menu"]/ul/menu-item[3]/li[1]'
WAFFLE_CHART = 'xpath = //*[@id="parent-menu"]/ul/menu-item[1]/li[1]'
MORE_ACTIONS2 = 'xpath = //*[@id="mat-tab-content-0-3"]/div/app-tech-parent/div[3]/div[1]/div[3]/card/div[2]/div/chart/charts-toggle-menu/action-menu/div[2]'
PIE_CHART2 = 'xpath=//*[@id="parent-menu"]/ul/menu-item[3]/li[1]'
WAFFLE_CHART2 = 'xpath=//*[@id="parent-menu"]/ul/menu-item[1]/li[1]'
DONUT_CHART2 = 'xpath=//*[@id="parent-menu"]/ul/menu-item[2]/li[1]'
