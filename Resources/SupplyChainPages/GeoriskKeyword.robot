*** Settings ***
Library  SeleniumLibrary
Variables     SupplyChainLocators.py
Variables    SupplyChainAssertion.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonVariables.py
Resource    ../../Resources/PartDetailPage/SpecificPartKeyword.robot
Resource    ../../Resources/PLNamePage/PLNameKeyword.robot
Resource    ../../Resources/ManufacturerPage/ManufacturerKeyword.robot
Resource    ../../Resources/SupplyChainPages/PartsListKeyword.robot
Resource    ../../Resources/DataManager.robot
Resource     ../../Resources/SharedPages/SidebarKeyword.robot

*** Variables ***
@{sites}=   ${FAB_SITE_PERCENT}   ${WAFER_TEST_PERCENT}    ${ASSEMBLY_SITE_PERCENT}    ${FINAL_TEST_PERCENT}
@{sites2}=   ${FAB_SITE_PERCENT2}   ${WAFER_TEST_PERCENT2}    ${ASSEMBLY_SITE_PERCENT2}    ${FINAL_TEST_PERCENT2}
${EXAMPLE_PERCENT}=  ${25}
${FLASH_TXT}=  flash
*** Keywords ***
enterEvents
    [Documentation]  "Allows for loading, then picks an event type and name. Then iterates through all buttons on tab"
    clickEventType
    selectType
    clickEventName
    selectName
    scrollToPCB
    fabButtonToggles
    fabButtonToggles2

clickEventType
    [Documentation]  "Clicks the dropdown event type menu."
    wait until element is visible  ${DROPDOWN_CONTENT}
    pause
    scroll element into view  ${SELECT_EVENT_TYPE}
    wait until element is visible  ${SELECT_EVENT_TYPE}
    pause
    Run Keyword until Success    click element   ${SELECT_EVENT_TYPE}

selectType
    [Documentation]  "CLicks the type desired from type drop down menu. Args: TypeSelected set to 'earthquakes' by default"
    [Arguments]  ${type_selected}=${EARTHQUAKES}
    #Could be any option in dropdown so static earthquakes is fine
    wait until element is visible  ${type_selected}
    Run Keyword until Success    click element   ${type_selected}
    wait until element is visible  ${DROPDOWN_CONTENT}
clickEventName
    [Documentation]  "Clicks event name drop-down menu"
    scroll element into view  ${SELECT_EVENT_NAME}
    wait until element is visible  ${SELECT_EVENT_NAME}
    pause
    Run Keyword until Success    click element   ${SELECT_EVENT_NAME}

selectName
    [Documentation]  "CLicks the specific event desired from 'event name' drop down menu. Args: nameSelected set to 'japan northeastern earthquake' by default"
    [Arguments]  ${name_selected}=${EARTHQUAKE2}
    wait until element is visible  ${name_selected}
    Run Keyword until Success    click element   ${name_selected}
scrollToPCB
    [Documentation]  "Scrolls so rest of buttons are in view"
    scroll element into view    ${PCB_CONTENT}
fabButtonToggles
    [Documentation]  "Clicks all button toggles in 'Manufacturer Risk'"
    clickMRasc
    clickMRdesc
    clickMRrisk
    clickMRnum
clickMRasc
    [Documentation]  "Clicks 'ascending' button in 'Manufacturer Risk'"
    wait until element is visible  ${MR_ASCENDING}
    Run Keyword until Success    click element   ${MR_ASCENDING}
    wait until element is visible  ${MR_CONTENT}
clickMRdesc
    [Documentation]  "Clicks 'descending' button in 'Manufacturer Risk'"
    wait until element is visible  ${MR_DESCENDING}
    Run Keyword until Success    click element   ${MR_DESCENDING}
    wait until element is visible  ${MR_CONTENT}
clickMRrisk
    [Documentation]  "Clicks 'High Risk' button in 'Manufacturer Risk'"
    wait until element is visible  ${MR_HIGHRISK_BTN}
    Run Keyword until Success    click element   ${MR_HIGHRISK_BTN}
    wait until element is visible  ${MR_CONTENT}
clickMRnum
    [Documentation]  "Clicks 'Number of Parts' button in 'Manufacturer Risk'"
    wait until element is visible  ${MR_NUM_PARTS_BTN}
    Run Keyword until Success    click element   ${MR_NUM_PARTS_BTN}
    wait until element is visible  ${MR_CONTENT}
fabButtonToggles2
    [Documentation]  "Clicks all button toggles in 'Product Category Breakdown'"
    clickAsc
    clickDesc
    clickRisk
    clickNum
clickAsc
    [Documentation]  "Clicks 'ascending' button in 'Manufacturer Risk'"
    wait until element is visible  ${ASCENDING_BTN}
    Run Keyword until Success    click element   ${ASCENDING_BTN}
    wait until element is visible  ${PCB_CONTENT}
clickDesc
    [Documentation]  "Clicks 'descending' button in 'Manufacturer Risk'"
    wait until element is visible  ${DESCENDING_BTN}
    Run Keyword until Success    click element   ${DESCENDING_BTN}
    wait until element is visible  ${PCB_CONTENT}
clickRisk
    [Documentation]  "Clicks 'High Risk' button in 'Manufacturer Risk'"
    wait until element is visible  ${HIGHRISK_BTN}
    Run Keyword until Success    click element   ${HIGHRISK_BTN}
    wait until element is visible  ${PCB_CONTENT}
clickNum
    [Documentation]  "Clicks 'Number of Parts' button in 'Manufacturer Risk'"
    wait until element is visible  ${NUM_PARTS_BTN}
    Run Keyword until Success    click element   ${NUM_PARTS_BTN}
    wait until element is visible  ${PCB_CONTENT}
clickWaferTest
    [Documentation]  "Clicks 'Wafer Test' tab"
    wait until element is visible  ${WAFER_TEST_TAB}
    Run Keyword until Success    click element   ${WAFER_TEST_TAB}
    wait until element is visible  ${MAP_CONTENT}
    element should contain    ${load_check}     ${load_check_text}
clickFabricationSite
    [Documentation]  "Clicks 'Fabrication Site' tab"
    wait until element is visible  ${FABRICATION_SITE_TAB}
    Run Keyword until Success    click element   ${FABRICATION_SITE_TAB}
    wait until element is visible  ${MAP_CONTENT}
    element should contain    ${load_check}     ${load_check_text}
clickAssemblySite
    [Documentation]  "Clicks 'Assembly Site' tab"
    wait until element is visible  ${ASSEMBLY_SITE_TAB}
    Run Keyword until Success    click element   ${ASSEMBLY_SITE_TAB}
    wait until element is visible  ${MAP_CONTENT}
    element should contain    ${load_check}     ${load_check_text}
clickFinaltestSite
    [Documentation]  "Clicks 'Final Test Site' tab"
    wait until element is visible  ${FINAL_TEST_TAB}
    Run Keyword until Success    click element   ${FINAL_TEST_TAB}
    wait until element is visible  ${MAP_CONTENT}
clickCountryOfOrigin
    [Documentation]  "Clicks 'Country of Origin' tab"
    wait until element is visible  ${COUNTRY_OF_ORIGIN_TAB}
    Run Keyword until Success    click element   ${COUNTRY_OF_ORIGIN_TAB}
    wait until element is visible  ${MAP_CONTENT}
    element should contain    ${load_check}     ${load_check_text}
loadingCompensation
    [Documentation]  "Pause to compensate loading times. Args: PauseTime is set to '12s' by default"
    [Arguments]  ${pause_time}= 8s
    sleep   ${pause_time}
    wait until element is visible    ${MAP_CONTENT}
clickGeoriskTab
    [Documentation]  "Clicks on the georisk tab"
    wait until element is visible   ${GEO_TAB_BTN}
    Run Keyword until Success    click element  ${GEO_TAB_BTN}
    wait until element is visible    ${LOAD_CHECK}
    element should contain    ${LOAD_CHECK}     ${LOAD_CHECK_TEXT}
click editable risk
    [Documentation]    "Clicks the editable risk menu to engage pop-up menu."
    wait until element is visible    ${EDITABLE_RISK}
    Run Keyword until Success    click element    ${EDITABLE_RISK}
enter percent
    [Documentation]    "Clears and enters desired pecent to 1 of the 4 sites that can be edited. Args: enter a site to change and a percent to input"
    [Arguments]    ${site_To_Change}  ${desired_Percent}
    wait until element is visible    ${site_To_Change}
    Run Keyword until Success    clear element text    ${site_To_Change}
    Run Keyword until Success    input text    ${site_To_Change}    ${desired_Percent}
    sleep    1s
save percents
    [Documentation]    "Clicks the app button on the editable risk menu."
    wait until element is visible   ${SAVE_BTN}
    Run Keyword until Success    click element    ${SAVE_BTN}
close editable risk
    [Documentation]    "Clicks the 'close' button in editable risk menu."
    wait until element is visible    ${EDITABLE_CLOSE}
    Run Keyword until Success    click element    ${EDITABLE_CLOSE}
    pause
verify percents
    [Documentation]    "Checks that a value in a specific text box is equal to a desired percent it should have been set to. Args: site_To_Change should be given the locator to text box, desired_percent should be the percent you expect to be in the box."
    [Arguments]    ${site_To_Change}  ${desired_Percent}
    wait until element is visible    ${site_To_Change}
    ${percent}=    get value    ${site_To_Change}
    should be equal as numbers    ${percent}    ${desired_Percent}
reset percents
    [Documentation]    "Clicks the 'x' button in the editable risk menu to close the popup."
    wait until element is visible    ${RESET_PERCENT}
    Run Keyword until Success    click element    ${RESET_PERCENT}
verify starter percents
    [Documentation]    "a 4x call to verify the default percents the georisk tab fills the editable risk menu with."
    verify percents    ${sites}[0]    15
    verify percents    ${sites}[1]    5
    verify percents    ${sites}[2]    70
    verify percents    ${sites}[3]    10
click reset filter
    [Documentation]    "Clicks the 'reset filter' option in the top right of the georisk tab.
    wait until element is visible    ${RESET_FILTER}
    Run Keyword until Success    click element    ${RESET_FILTER}
click show parts
    [Documentation]    "Clicks the 'show parts' button in the top right of the georisk tab."
    wait until element is visible    ${SHOW_PARTS}
    Run Keyword until Success    click element    ${SHOW_PARTS}
geo click select bom
    [Documentation]    "Clicks the 'Select BOM' option in the top right of the georisk tab."
    wait until element is visible    ${SELECT_BOM}
    Run Keyword until Success    click element    ${SELECT_BOM}
geo click select bom search
    [Documentation]    "Clicks the search box on the 'Select BOM' pop-up."
    wait until element is visible    ${SELECT_BOM_SEARCH_BOX}
    Run Keyword until Success    click element    ${SELECT_BOM_SEARCH_BOX}

geo enter bom to search
    [Documentation]    "Clears text box then enter text to search. Args: test is set to 'Test' by default."
    [Arguments]    ${text}=Test
    clear element text    ${SELECT_BOM_SEARCH_BOX}
    Run Keyword until Success    input text    ${SELECT_BOM_SEARCH_BOX}    ${text}
    Run Keyword until Success    click element    ${SELECT_BOM_SEARCH_BTN}
geo reset select bom
    [Documentation]    "Clicks the 'reset' button on 'Select Bom' pop-up."
    wait until element is visible    ${SELECT_BOM_RESET}
    Run Keyword until Success    click element    ${SELECT_BOM_RESET}
geo choose BOM
    [Documentation]    "Clicks desired BOM from the 'Select BOM' pop-up."
    [Arguments]    ${bom}=${SELECT_BOM_TEST}
    wait until element is visible   ${bom}
    Run Keyword until Success    click element    ${bom}
    wait until element is visible    ${SELECT_BOM_CONTENT}
geo close choose bom
    [Documentation]    "Closes the 'Select BOM' pop-up."
    wait until element is visible    ${SELECT_BOM_CLOSE}
    Run Keyword until Success    click element    ${SELECT_BOM_CLOSE}
dropdown sequence
    [Arguments]  ${TypeSelected}  ${NameSelected}
    clickSupplyChain
    loadingCompensation
    clickEventType
    selectType     ${TypeSelected}
    clickEventName
    selectName     ${NameSelected}
    pause
    click reset filter
edit risk sequence
    clickSupplyChain
    click editable risk
    FOR    ${site}    IN    @{sites}
        enter percent    ${site}    ${EXAMPLE_PERCENT}
    END
    save percents
    close editable risk
    click editable risk
    FOR    ${site2}    IN    @{sites2}
        verify percents    ${site2}    ${EXAMPLE_PERCENT}
    END
    close editable risk
    click editable risk
    reset percents
    save percents
    close editable risk
    logout action
    access silicon expert
    clickSupplyChain
    pause
    pause
    pause
    click editable risk
    verify starter percents
    close editable risk
show part sequence
    clickSupplyChain
    click show parts
    search part
    click view
    click part number
    confirm part number
    click manufacturer
    confirm manufacturer
    click pl name
    confirm pl name
    back from map
select bom sequence
    geo click select bom
    pause
    geo click select bom search
    pause
    geo enter bom to search
    geo reset select bom
    geo choose BOM    ${TEST_BOM_XPATH}
    geo click select bom
    geo close choose bom