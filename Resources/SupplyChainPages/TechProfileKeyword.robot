*** Settings ***
Library  SeleniumLibrary
Variables     SupplyChainLocators.py
Variables    SupplyChainAssertion.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
*** Variables ***

*** Keywords ***
techFeatures
    [Documentation]  "Executes all features on technoligical profile tab"
    choosePie
    chooseDonut
    chooseWaffle
    choosePie2
    chooseDonut2
    chooseWaffle2
    fixView
    techSearch

clickMoreActions
    [Documentation]  "Clicks the three dot menu on 'MFR Production of Active Parts' that allows chart toggle"
    wait until element is visible  ${MORE_ACTIONS}
    Run Keyword until Success    click element  ${MORE_ACTIONS}
choosePie
    [Documentation]  "Chooses 'pie chart' option from more actions menu"
    clickMoreActions
    wait until element is visible   ${PIE_CHART}
    Run Keyword until Success    click element  ${PIE_CHART}
    pause
chooseDonut
    [Documentation]  "Chooses 'donut chart' option from more actions menu"
    clickMoreActions
    wait until element is visible   ${DONUT_CHART}
    Run Keyword until Success    click element  ${DONUT_CHART}
    pause
chooseWaffle
    [Documentation]  "Chooses 'waffle chart' option from more actions menu"
    clickMoreActions
    wait until element is visible   ${WAFFLE_CHART}
    Run Keyword until Success    click element  ${WAFFLE_CHART}
    pause
clickMoreActions2
    [Documentation]  "Clicks three dot menu on 'Trend of Active Parts' that allows chart toggle"
    wait until element is visible  ${MORE_ACTIONS2}
    Run Keyword until Success    click element  ${MORE_ACTIONS2}
choosePie2
    [Documentation]  "Chooses 'pie chart' option from more actions 2 menu"
    clickMoreActions2
    wait until element is visible   ${PIE_CHART2}
    Run Keyword until Success    click element  ${PIE_CHART2}
    pause
chooseDonut2
    [Documentation]  "Chooses 'donut chart' option from more actions 2 menu"
    clickMoreActions2
    wait until element is visible   ${DONUT_CHART2}
    Run Keyword until Success    click element  ${DONUT_CHART2}
    pause
fixView
    [Documentation]  "Scrolls down to allow better view of window"
    scroll element into view  ${TECH_FOOTER}
chooseWaffle2
    [Documentation]  "Chooses 'waffle chart' option from more actions 2 menu"
    clickMoreActions2
    wait until element is visible   ${WAFFLE_CHART2}
    Run Keyword until Success    click element  ${WAFFLE_CHART2}
    pause
techSearch
    [Documentation]  "Enters and searches text in the tech profile search box. Args: techSearchText set to 'vishay' by default"
    [Arguments]  ${tech_search_text}=${VISHAY}
    wait until element is visible  ${TECH_SEARCH_BOX}
    Run Keyword until Success    click element  ${TECH_SEARCH_BOX}
    Run Keyword until Success    input text  ${TECH_SEARCH_BOX}    ${tech_search_text}
    press keys  ${TECH_SEARCH_BOX}    ${ENTER}
    pause
clickTechProfTab
    [Documentation]  "Clicks tech profile tab from other supply chain tabs"
    wait until element is visible   ${TECH_PROF_TAB_BTN}
    Run Keyword until Success    click element  ${TECH_PROF_TAB_BTN}
click select bom
    [Documentation]    "Clicks 'Select BOM' button on top right of page."
    wait until element is visible    ${SELECT_BOM}
    Run Keyword until Success    click element    ${SELECT_BOM}
click select bom search
    [Documentation]    "Clicks search bar on 'Select BOM' pop-up."
    wait until element is visible    ${SELECT_BOM_SEARCH_BOX}
    Run Keyword until Success    click element    ${SELECT_BOM_SEARCH_BOX}
enter bom to search
    [Documentation]    "Enters desired text into search bar on 'Select BOM' pop-up."
    [Arguments]    ${text}=Test
    clear element text    ${SELECT_BOM_SEARCH_BOX}
    Run Keyword until Success    input text    ${SELECT_BOM_SEARCH_BOX}    ${text}
    Run Keyword until Success    click element    ${SELECT_BOM_SEARCH_BTN}
reset select bom
    [Documentation]    "clicks 'reset' button from 'Select BOM' pop-up."
    wait until element is visible    ${SELECT_BOM_RESET}
    Run Keyword until Success    click element    ${SELECT_BOM_RESET}
choose BOM
    [Documentation]    "Clicks desired BOM from 'Select BOM' pop-up. Args: bom is set to 'Test' by default"
    [Arguments]    ${bom}=${SELECT_BOM_TEST}
    wait until element is visible   ${bom}
    Run Keyword until Success    click element    ${bom}
    wait until element is visible    ${MORE_ACTIONS}
close choose bom
    [Documentation]    "Closes the 'Select BOM' pop-up"
    wait until element is visible    ${SELECT_BOM_CLOSE}
    Run Keyword until Success    click element    ${SELECT_BOM_CLOSE}