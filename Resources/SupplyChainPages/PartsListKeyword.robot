*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Resource   ../../Resources/PartDetailPage/SpecificPartKeyword.robot
Resource    ../../Resources/ManufacturerPage/ManufacturerKeyword.robot
Resource    ../../Resources/PLNamePage/PLNameKeyword.robot
Variables     SupplyChainLocators.py
Variables    SupplyChainAssertion.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonVariables.py
Variables    ../CommonKeywords/CommonAssertion.py

*** Variables ***
*** Keywords ***
search part
    [Documentation]    "enters desired part text into the search bar on the 'search_part' page. Args: part to search is set to QCA9882 part by default"
    [Arguments]    ${searchPart}=QCA9882
    wait until element is visible    ${SEARCH_BOX_PARTS}
    wait until element is visible    ${VIEW_MAP}
    Run Keyword until Success    click element    ${SEARCH_BOX_PARTS}
    Run Keyword until Success    input text    ${SEARCH_BOX_PARTS}    ${searchPart}
click view
    [Documentation]    "Clicks a 'view map' in the chart to demonstrate functionality. Could be updated to take an argument."
    wait until element is visible    ${VIEW_MAP}
    Run Keyword until Success    click element    ${VIEW_MAP}
back from map
    [Documentation]    "Clicks the back button that appears once a user has clicked 'view map'"
    scroll element into view    ${SHOW_PARTS_BACK}
    wait until element is visible    ${SHOW_PARTS_BACK}
    Run Keyword until Success    click element    ${SHOW_PARTS_BACK}
click part number
    [Documentation]    "Clicks a part in the chart. Args: part is set to 'QCA9882' link"
    [Arguments]    ${part}=${QCA9882}
    scroll element into view    ${part}
    wait until element is visible  ${part}
    Run Keyword until Success    click element    ${part}
confirm part number
    [Documentation]    "Ensures the part page loads."
    Verify Part Page loaded sucss

click manufacturer
    [Documentation]    "Clicks a manufacturer from the chart. Args: manu is set to 'Qualcomm' link by default, to go with QCA9882
    [Arguments]    ${manu}=${QUALCOMM}
    scroll element into view    ${manu}
    wait until element is visible    ${manu}
    Run Keyword until Success    click element    ${manu}
confirm manufacturer
    [Documentation]    "Ensures the manufacturer page loads"
    Verify Manu Page loaded sucss
click pl name
    [Documentation]    "Clicks a 'PL name' from the chart. Args: pl_name is set to 'Wireless Lan' link by default."
    [Arguments]    ${pl_name}=${WIRELESS_LAN}
    wait until element is visible    ${pl_name}
    Run Keyword until Success    click element    ${pl_name}
confirm pl name
    [Documentation]   "confirms that the pl name page loads."
    Verify PL Page loaded sucss