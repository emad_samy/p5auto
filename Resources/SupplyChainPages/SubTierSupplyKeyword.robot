*** Settings ***
Library  SeleniumLibrary
Resource     ../../Resources/PLNamePage/PLNameKeyword.robot
Variables     SupplyChainLocators.py
Variables    SupplyChainAssertion.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py


*** Variables ***

*** Keywords ***
stsFeatures
    [Documentation]  "Executes all events on STS tab"
    stsSearch
    stsDropdownSelect
    stsMineralBtns
    stsReset
stsSearch
    [Documentation]  "Utilizes the search bar on STS tab with minor pauses to compensate loading times. Args: stsSearchText is set to 'vishay' by default"
    [Arguments]  ${sts_search_text}=${VISHAY}
    Run Keyword until Success    click element  ${STS_SEARCH_BOX}
    Run Keyword until Success    input text  ${STS_SEARCH_BOX}    ${sts_search_text}
    pause
    press keys  ${STS_SEARCH_BOX}    ${ENTER}
    pause
    pause
stsMineralBtns
    [Documentation]  "Toggles all the buttons in the mineral pricing box"
    clickDaily
    clickWeekly
    clickMonthly
clickDaily
    [Documentation]  "Clicks the 'Daily' button"
    scroll element into view  ${STS_FOOTER}
    Run Keyword until Success    click element  ${DAILY_BTN}
    wait until element is visible  ${MINERAL_CONTENT}
clickMonthly
    [Documentation]  "Clicks the 'Monthly' button"
    scroll element into view  ${STS_FOOTER}
    Run Keyword until Success    click element  ${MONTHLY_BTN}
    wait until element is visible  ${MINERAL_CONTENT}
clickWeekly
    [Documentation]  "Clicks the 'Weekly' button"
    scroll element into view  ${STS_FOOTER}
    Run Keyword until Success    click element  ${WEEKLY_BTN}
    wait until element is visible  ${MINERAL_CONTENT}
stsDropdownSelect
    [Documentation]  "Full dropdown selection sequence"
    #[Arguments]  ""
    clickMineralMenu
    clickMineral
clickMineralMenu
    [Documentation]  "Click the dropdown menu after scrolling to bottom of page"
    scroll element into view  ${STS_FOOTER}
    wait until element is visible  ${MINERAL_MENU}
    Run Keyword until Success    click element  ${MINERAL_MENU}
clickMineral
    [Documentation]  "Click the  desired Mineral. Args: ChosenMineral is set to gold by default
    [Arguments]  ${chosen_mineral}=${GOLD}
    wait until element is visible  ${ANY_MINERAL}
    Run Keyword until Success    click element  ${chosen_mineral}
    wait until element is visible  ${MINERAL_CONTENT}

stsReset
    [Documentation]  "Clicks the button that Resets the STS tab"
    scroll element into view  ${RESET_BTN}
    Run Keyword until Success    click element  ${RESET_BTN}
clickSTSTab
    [Documentation]  "Clicks Sts tab from other supply chain tabs"
    wait until element is visible   ${STS_TAB_BTN}
    Run Keyword until Success    click element  ${STS_TAB_BTN}
click select bom
    [Documentation]    "Clicks 'Select BOM' button on top right of page."
    wait until element is visible    ${SELECT_BOM}
    Run Keyword until Success    click element    ${SELECT_BOM}
click select bom search
    [Documentation]    "Clicks search bar on 'Select BOM' pop-up."
    wait until element is visible    ${SELECT_BOM_SEARCH_BOX}
    Run Keyword until Success    click element    ${SELECT_BOM_SEARCH_BOX}
enter bom to search
    [Documentation]    "Enters desired text into search bar on 'Select BOM' pop-up."
    [Arguments]    ${text}=Test
    clear element text    ${SELECT_BOM_SEARCH_BOX}
    Run Keyword until Success    input text    ${SELECT_BOM_SEARCH_BOX}    ${text}
    Run Keyword until Success    click element    ${SELECT_BOM_SEARCH_BTN}
reset select bom
    [Documentation]    "clicks 'reset' button from 'Select BOM' pop-up."
    wait until element is visible    ${SELECT_BOM_RESET}
    Run Keyword until Success    click element    ${SELECT_BOM_RESET}
choose BOM
    [Documentation]    "Clicks desired BOM from 'Select BOM' pop-up. Args: bom is set to 'Test' by default"
    [Arguments]    ${bom}=${SELECT_BOM_TEST}
    wait until element is visible   ${bom}
    Run Keyword until Success    click element    ${bom}
    wait until element is visible    ${STS_SEARCH_BOX }
close choose bom
    [Documentation]    "Closes the 'Select BOM' pop-up"
    wait until element is visible    ${SELECT_BOM_CLOSE}
    Run Keyword until Success    click element    ${SELECT_BOM_CLOSE}