*** Settings ***
Library  SeleniumLibrary
Variables     SupplyChainLocators.py
Variables    SupplyChainAssertion.py
Resource   ../CommonKeywords/CommonKeyword.robot
Variables    ../CommonKeywords/CommonVariables.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
*** Variables ***

*** Keywords ***
useFeatures
    [Documentation]  "Toggles through the Insight buttons, executes a market search, toggles through mpn grid buttons"
    marketButtons
    marketSearch
    mpnButtons
marketButtons
    [Documentation]  "Clicks through the insights sidebar"
    clickPrice
    clickStock
    clickLead
    #clickDate
mpnButtons
    [Documentation]  "Clicks through mpn grid options"
    clickSolved
    clickUn
    clickViewAll
    #clickApply
clickPrice
    [Documentation]  "Clicks the 'Price' button"
    wait until element is visible  ${PRICE_BTN}
    Run Keyword until Success    click button  ${PRICE_BTN}
    wait until element is visible    ${MPN_CONTENT}
clickStock
    [Documentation]  "Clicks the 'Stock' button"
    wait until element is visible  ${STOCK_BTN}
    Run Keyword until Success    click button  ${STOCK_BTN}
    wait until element is visible    ${MPN_CONTENT}
clickLead
    [Documentation]  "Clicks the 'Lead Time' button"
    wait until element is visible  ${LEAD_TIME_BTN}
    Run Keyword until Success    click button  ${LEAD_TIME_BTN}
    wait until element is visible    ${MPN_CONTENT}

    #wait until element is visible  xpath = //*[@id="mat-tab-content-0-1"]/div/app-forecast-parent/div[2]/div/div/div[2]/app-news-feed-cards/div/mat-card/div[3]/div[1]
clickDate
    [Documentation]  "Clicks the 'Date' button."
    wait until element is visible  ${DATE_BTN}
    Run Keyword until Success    click button  ${DATE_BTN}
    wait until element is visible    ${MPN_CONTENT}
clickSolved
    [Documentation]  "Click the 'Solved' button and briefly pauses to account for load times."
    wait until element is visible  ${SOLVED_BTN}
    Run Keyword until Success    click button  ${SOLVED_BTN}
    pause
clickUn
    [Documentation]  "Click the 'Unsolved' button and briefly pauses to account for load times."
    wait until element is visible  ${UNSOLVED_BTN}
    Run Keyword until Success    click button  ${UNSOLVED_BTN}
    pause
clickViewAll
    [Documentation]  "Click the 'View All' button and pauses for a significant loading compensation time."
    wait until element is visible  ${VIEW_ALL_BTN}
    Run Keyword until Success    click button  ${VIEW_ALL_BTN}
    pause
    wait until element is visible    ${MPN_CONTENT}
#    wait until element is visible  ${MPN_CONTENT}
    #load
    #wait until element is visible  xpath = //*[@id="mat-tab-content-0-1"]/div/app-forecast-parent/div[2]/div/div/div[1]/div[1]/app-part-grid-table/mat-card/div[3]/mat-table
clickApply
    [Documentation]  "Clicks the 'Apply Selection' button."
    wait until element is visible  ${APPLY_BTN}
    Run Keyword until Success    click button  ${APPLY_BTN}
    pause
marketSearch
    [Documentation]  "Executes a search over the mpn grid. Args: SearchText is set to 'flash' as default search test."
    [Arguments]  ${search_text}= flash
    wait until element is visible  ${MPN_CONTENT}
    Run Keyword until Success    click element  ${SEARCH_BOX}
    Run Keyword until Success    input text  ${SEARCH_BOX}    ${search_text}
    Run Keyword until Success    press keys  ${SEARCH_BOX}    ${ENTER}
    wait until element is visible  ${MPN_CONTENT}
clickMarketTab
    [Documentation]  "Clicks the market forecast tab from other supply chain tabs."
    wait until element is visible   ${MARKET_TAB_BTN}
    Run Keyword until Success    click element  ${MARKET_TAB_BTN}
load
    [Documentation]  "A significant sleep to account for mpn grid's load times. Args: LoadingTime is set to 19s by default."
    [Arguments]  ${loading_time}=${LOAD_TIME}
    sleep   ${loading_time}
click select bom
    [Documentation]    "Clicks 'Select BOM' button on top right of page."
    wait until element is visible    ${SELECT_BOM}
    Run Keyword until Success    click element    ${SELECT_BOM}
click select bom search
    [Documentation]    "Clicks search bar on 'Select BOM' pop-up."
    wait until element is visible    ${SELECT_BOM_SEARCH_BOX}
    Run Keyword until Success    click element    ${SELECT_BOM_SEARCH_BOX}

enter bom to search
    [Documentation]    "Enters desired text into search bar on 'Select BOM' pop-up."
    [Arguments]    ${text}=Test
    clear element text    ${SELECT_BOM_SEARCH_BOX}
    Run Keyword until Success    input text    ${SELECT_BOM_SEARCH_BOX}    ${text}
    Run Keyword until Success    click element    ${SELECT_BOM_SEARCH_BTN}
reset select bom
    [Documentation]    "clicks 'reset' button from 'Select BOM' pop-up."
    wait until element is visible    ${SELECT_BOM_RESET}
    Run Keyword until Success    click element    ${SELECT_BOM_RESET}
choose BOM
    [Documentation]    "Clicks desired BOM from 'Select BOM' pop-up. Args: bom is set to 'Test' by default"
    [Arguments]    ${bom}=${SELECT_BOM_TEST}
    wait until element is visible   ${bom}
    Run Keyword until Success    click element    ${bom}
    wait until element is visible    ${MPN_CONTENT}
close choose bom
    [Documentation]    "Closes the 'Select BOM' pop-up"
    wait until element is visible    ${SELECT_BOM_CLOSE}
    Run Keyword until Success    click element    ${SELECT_BOM_CLOSE}
open filter
    [Documentation]    "Opens the filter menu with a brief pause to accomadate loading times."
    wait until element is visible    ${FILTER_BTN}
    Run Keyword until Success    click element    ${FILTER_BTN}
click manu
    [Documentation]    "clicks search box in filter menu."
    wait until element is visible    ${MANU_SEARCH}
    Run Keyword until Success    click element    ${MANU_SEARCH}
choose manu
    [Documentation]    "Clicks a manufacturer from search drop-down. Args: manu is set to '3M' by default."
    [Arguments]    ${manu}=${THREE_M}
    scroll element into view    ${manu}
    wait until element is visible    ${manu}
    Run Keyword until Success    click element    ${manu}
choose dates
    [Documentation]    "Chooses a 'to' and 'from' date. Args: start date is set to June 1 by default, and end date defaults to June 30.
    [Arguments]    ${start_date}=${June_1}   ${end_date}=${June_30}
    wait until element is visible    ${FROM_DATE}
    Run Keyword until Success    click element    ${FROM_DATE}
    wait until element is visible    ${start_date}
    Run Keyword until Success    click element    ${start_date}
    wait until element is visible    ${TO_DATE}
    Run Keyword until Success    click element    ${TO_DATE}
    wait until element is visible    ${end_date}
    Run Keyword until Success    click element    ${end_date}
click price increase
    [Documentation]    "Clicks 'Price Increase' button in filter menu."
    wait until element is visible    ${PRICE_INC}
    Run Keyword until Success    click element    ${PRICE_INC}
click price decrease
    [Documentation]    "Clicks 'Price Decrease' button in filter menu."
    wait until element is visible    ${PRICE_DEC}
    Run Keyword until Success    click element    ${PRICE_DEC}
click inventory increase
    [Documentation]    "Clicks 'Inventory Increase' button in filter menu."
    wait until element is visible    ${INV_INC}
    Run Keyword until Success    click element    ${INV_INC}
click inventory decrease
    [Documentation]    "Clicks 'Inventory Decrease' button in filter menu."
    wait until element is visible    ${INV_DEC}
    Run Keyword until Success    click element    ${INV_DEC}
click lead time increase
    [Documentation]    "Clicks 'Lead Time Increase' button in filter menu."
    wait until element is visible    ${LEAD_INC}
    Run Keyword until Success    click element    ${LEAD_INC}
click lead time decrease
    [Documentation]    "Clicks 'Lead Time Decrease' button in filter menu."
    wait until element is visible    ${LEAD_DEC}
    Run Keyword until Success    click element    ${LEAD_DEC}
apply filter
    [Documentation]    "Clicks 'Apply' option in filter menu."
    wait until element is visible    ${APPLY_FILTER}
    Run Keyword until Success    click element    ${APPLY_FILTER}
reset filter
    [Documentation]    "Clicks 'reset' button in filter menu."
    wait until element is visible    ${RESET_FILTER}
    Run Keyword until Success    click element    ${RESET_FILTER}
    pause
market search sequence
    clickSupplyChain
    pause
    clickMarketTab
    [Documentation]  "Test of the MPN grid search feature"
    ${csv_data}=    get csv data    ${MARKET_CSV_FILE}
    FOR    ${index}     IN RANGE    3
        ${text_to_search}=    set variable    ${csv_data[${index}]}
        run keyword and continue on failure    marketSearch    ${text_to_search}[0]
    END
apply filter sequence
    clickMarketTab
    open filter
    click manu
    choose manu
    choose dates
    click price increase
    click price decrease
    click inventory increase
    click inventory decrease
    click lead time increase
    click lead time decrease
    apply filter
reset filter sequence
    clickMarketTab
    open filter
    click manu
    choose manu
    choose dates
    click price increase
    apply filter
    pause
    reset filter