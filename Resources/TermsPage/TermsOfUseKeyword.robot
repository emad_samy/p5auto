*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Variables     TermsLocators.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
*** Variables ***
*** Keywords ***
handle terms
    [Documentation]  "Goes back to SE main page"
    switch window    new
    pause
    wait until element is visible   ${TERMS_CONTENT}
    switch window  main