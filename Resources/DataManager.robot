#next time, but dont need to put it in a directory
#used as library in other files as resource to show how to use data.
*** Settings ***
Library    ../CustomLibrary/CustomLib.py
Library     DatabaseLibrary
Library    OperatingSystem
Variables    CommonKeywords/CommonAssertion.py
Variables    CommonKeywords/CommonLocators.py
Variables    CommonKeywords/CommonVariables.py
*** Variables ***
${FILE_TO_BE_USED}=    ..\\p5auto\\TestData\\georisk.csv
${XLS_FILE_TO_BE_USED}=    ..\\p5auto\\TestData\\ConstantSearch.xlsx


*** Keywords ***
connect to mySQL
    [Arguments]    ${db_name}=${DB_NAME}   ${db_user}=${DB_USER}    ${db_pw}=${DB_PASSWORD}   ${db_host}=${DB_HOST}   ${db_port}=${DB_PORT}
    #five parm + name of db
    log to console    ${db_name}
    log to console    ${db_user}
    log to console    ${db_pw}
    log to console    ${db_host}
    log to console    ${db_port}
    connect to database    pymysql    ${db_name}   ${db_user}    ${db_pw}   ${db_host}   ${db_port}

execute db query
    ${USERS}=    databaselibrary.row count     select * from users where username = 'liam.calnan@siliconexpert.com'
    log to console    ${USERS}



get csv data
    [Arguments]    ${file_name}=${FILE_TO_BE_USED}
    ${data}=    read csv file    ${file_name}
    [return]    ${data}
get xls data
    [Arguments]    ${file_name}=${XLS_FILE_TO_BE_USED}
    ${data}=    read xls file    ${file_name}
    [return]    ${data}
get xml data
    [Arguments]    ${file_name}=${XML_FILE_TO_BE_USED}
    ${data}=    read xml file    ${file_name}
    [return]    ${data}
