*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/PrivacyPage/PrivacyPolicyKeyword.robot
Resource    ../../Resources/TermsPage/TermsOfUseKeyword.robot
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
Variables     SharedPagesLocators.py
*** Variables ***
*** Keywords ***
click terms
    [Documentation]    "Clicks the terms option at footer of page. Then calls handle terms from Linked Pages/TermsOfUseKeyword.robot"
    scroll element into view  ${TERMS_OF_USE}
    Run Keyword until Success    click element  ${TERMS_OF_USE}
    handle terms
    wait until element is visible  ${PROFILE_BTN}
click privacy
    [Documentation]    "Clicks the privacy and cookie policy option in the footer of the page. Then returns to the main page."
    scroll element into view  ${PRIVACY_AND_COOKIE_POLICY}
    Run Keyword until Success    click element  ${PRIVACY_AND_COOKIE_POLICY}
    #wait until element is visible   ${Privacy_Content}
    handle privacy
    wait until element is visible  ${PROFILE_BTN}
click contact us
    [Documentation]    "Clicks the privacy policy option in the footer of the page. Then returns to the main page."
    scroll element into view  ${CONTACT_US}
    Run Keyword until Success    click element  ${CONTACT_US}
    #wait until element is visible   ${Privacy_Content}
    pause
    switch window  main
    wait until element is visible  ${PROFILE_BTN}