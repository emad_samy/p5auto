*** Settings ***
Library  SeleniumLibrary
Resource    ../CommonKeywords/CommonKeyword.robot
Variables     SharedPagesLocators.py
Variables    SharedPagesAssertion.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py

*** Variables ***
*** Keywords ***
click home
    [Documentation]    "Clicks the 'Home' option in the sidebar."
    scroll element into view  ${HOME_BTN}
    wait until element is visible    ${HOME_BTN}
    Run Keyword until Success    click element  ${HOME_BTN}
    wait until element is visible   ${HOME_CONTENT}
    element should contain    ${HOME_CONTENT_CHECK}    ${HOME_CONTENT_TEXT}
click acl
    [Documentation]    "Clicks the 'ACL' (approved components list) option in the sidebar."
    scroll element into view  ${ACL_BTN}
    Run Keyword until Success    click element  ${ACL_BTN}
    wait until element is visible   ${ACL_CONTENT}
    element should contain    ${ACL_HEADER}    ${ACL_HEADER_TEXT}
click browse
    [Documentation]    "Clicks the 'Browse' option in the sidebar."
    scroll element into view  ${BROWSE_BTN}
    Run Keyword until Success    click element  ${BROWSE_BTN}
    wait until element is visible   ${BROWSE_CONTENT}
    element should contain    ${BROWSE_CONTENT_CHECK}    ${BROWSE_CONTENT_TEXT}
click scip report
    [Documentation]    "Clicks the 'SCIP Reports' option in the sidebar."
    wait until element is visible   ${SCIP_REPORTS_BTN}
    Run Keyword until Success    click element  ${SCIP_REPORTS_BTN}
    wait until element is visible   ${SCIP_CONTENT}
    element should contain    ${SCIP_CONTENT_CHECK}    ${SCIP_CONTENT_TEXT}
click my boms
    [Documentation]    "Click 'My BOMs' option in sidebar."
    wait until element is visible   ${MY_BOMS_BTN}
    Run Keyword until Success    click element  ${MY_BOMS_BTN}
    wait until element is visible   ${MY_BOMS_CONTENT}
click add boms
    [Documentation]    "Clicks the plus icon next to the 'My BOMs' option in the sidebar."
    wait until element is visible   ${ADD_BOM_BTN}
    Run Keyword until Success    click element  ${ADD_BOM_BTN}
    wait until element is visible   ${NEW_EMPTY_BOM}
click new empty bom
    [Documentation]    "Clicks 'new empty BOM' option from add BOM pop-up menu."
    Run Keyword until Success    click element  ${NEW_EMPTY_BOM}
    wait until element is visible  ${NEW_EMPTY_CONTENT}
    Run Keyword until Success    click element  ${CLOSE_NEB}
click upload new bom
    [Documentation]    "Clicks the 'upload new BOM' option from add BOM pop-up menu."
    Run Keyword until Success    click element  ${UPLOAD_NEW_BOM}
    wait until element is visible  ${UPLOAD_NEW_CONTENT}
    Run Keyword until Success    click element  ${CLOSE_UNB}
click new project
    [Documentation]    "Clicks the 'new project' option from the add BOM pop-up menu"
    Run Keyword until Success    click element  ${NEW_PROJECT}
    wait until element is visible  ${NEW_CONTENT}
    Run Keyword until Success    click element  ${CLOSE_NEW_PROJECT}
BOM Search Miss
    [Documentation]    "Function that is called to simulate a known non returning result."
    wait until element is visible  ${BOM_SEARCH}
    Run Keyword until Success    input text  ${BOM_SEARCH}   ${SAMPLE_BOM_TEXT2}
    pause
    Run Keyword until Success    press keys  ${BOM_SEARCH}    ${ENTER}
    pause
    Run Keyword until Success    clear element text  ${BOM_SEARCH}
BOM Search
    [Documentation]  "Searches in sidebar for BOMS available to user. Args: text set to 'test' as default"
    [Arguments]  ${search_text}=${SAMPLE_BOM_TEXT}
    wait until element is visible  ${BOM_SEARCH}
    Run Keyword until Success    clear element text  ${BOM_SEARCH}
    Run Keyword until Success    input text  ${BOM_SEARCH}   ${search_text}
    pause
    Run Keyword until Success    press keys  ${BOM_SEARCH}    ${ENTER}
click test Bom
    [Documentation]    "Clicks one of the BOMs in users sidebar. Args: bom_to_click is set to 'Test' as default."
    [Arguments]    ${bom_to_click}=${TEST_BOM}
    wait until element is visible  ${bom_to_click}
    Run Keyword until Success    click element  ${bom_to_click}
    wait until element is visible  ${TEST_BOM_CONTENT}
clickSupplyChain
    [Documentation]  "Clicks supplychain button on left sidebar"
    wait until element is visible  ${SUPPLY_CHAIN_BTN}
    Run Keyword until Success    click element  ${SUPPLY_CHAIN_BTN}
    element should contain    ${SUPPLY_CHAIN_CONTENT}    ${SUPPLY_CHAIN_TEXT}