*** Settings ***
Library  SeleniumLibrary
Resource    ../../Resources/AlertManagerPage/AlertManagerKeyword.robot
Variables     SharedPagesLocators.py
Variables    ../CommonKeywords/CommonLocators.py
Variables    ../CommonKeywords/CommonAssertion.py
Variables    ../CommonKeywords/CommonVariables.py
*** Variables ***
*** Keywords ***
logout
    [Documentation]  "3 steps to log out user"
    clickProfile
    clickSignOut
    closeWindow
click logo
    [Documentation]  "Clicks SE logo in top left"
    wait until element is visible  ${SILICON_EXPERT_LOGO}
    Run Keyword until Success    click element  ${SILICON_EXPERT_LOGO}
    wait until element is visible  ${HOME_CONTENT}
closeWindow
    [Documentation]  "Closes browser window"
    close browser
clickProfile
    [Documentation]  "Clicks profile icon in top right"
    wait until element is visible  ${PROFILE_BTN}
    Run Keyword until Success    click element  ${PROFILE_BTN}
    wait until element is visible  ${SIGNOUT_BTN}
click account
    [Documentation]  "Click 'Account' option that appears from clickProfile"
    wait until element is visible  ${PROFILE_ACCOUNT}
    Run Keyword until Success    click element  ${PROFILE_ACCOUNT}
    wait until element is visible  ${PROFILE_ACCOUNT_CONTENT}
close account
    [Documentation]  "Closes 'Account' page"
    Run Keyword until Success    click element  ${PROFILE_ACCOUNT_CLOSE}
    wait until element is visible  ${PROFILE_BTN}
click profile submit a ticket
    [Documentation]  "Click 'Submit A Ticket' option that appears from clickProfile"
    wait until element is visible  ${PROFILE_ACCOUNT}
    Run Keyword until Success    click element  ${PROFILE_SUBMIT_A_TICKET}
    #wait until element is visible  ${Submit_A_Ticket_Content}
    pause
    pause
    switch window  main
    wait until element is visible  ${PROFILE_BTN}
click submit a ticket
    [Documentation]  "Click 'Submit A Ticket' option in banner"
    wait until element is visible  ${SUBMIT_A_TICKET}
    Run Keyword until Success    click element  ${Submit_A_Ticket}
    #wait until element is visible  ${Submit_A_Ticket_Content}
    pause
    pause
    switch window  main
    wait until element is visible  ${PROFILE_BTN}
click alerts
    [Documentation]  "Click 'Alerts' option from banner"
    wait until element is visible  ${ALERT_MANAGER}
    Run Keyword until Success    click element  ${ALERT_MANAGER}
    Verify Alert Manager Page loaded sucss
click change language
    [Documentation]  "Click 'change language' option that appears from clickProfile"
    wait until element is visible  ${PROFILE_ACCOUNT}
    Run Keyword until Success    mouse over  ${PROFILE_CHANGE_LANGUAGE}
    wait until element is visible  ${FIRST_LANGUAGE}
choose language
    [Documentation]  "Clicks desired language. Args: language is set to English as default.
    [Arguments]  ${language}=${ENGLISH}
    wait until element is visible  ${FIRST_LANGUAGE}
    Run Keyword until Success    click element  ${language}
search for part
    [Documentation]  "Search for a part from search box in banner. Args: default text is 'a'"
    [Arguments]  ${search_text}=a
    wait until element is visible  ${PART_SEARCH_BOX}
    Run Keyword until Success    click element  ${PART_SEARCH_BOX}
    Run Keyword until Success    clear element text  ${PART_SEARCH_BOX}
    Run Keyword until Success    input text  ${PART_SEARCH_BOX}  ${search_text}
    Run Keyword until Success    click element  ${PART_SEARCH_BTN}
    wait until element is visible  ${SAMPLE_PART_CONTENT}
use search suggestion
    [Documentation]  "Clicks the suggested part that comes up after some text is inputed. Args: text is set to 'a' by default, part is set to the first part that pops up."
    [Arguments]  ${text}=a    ${part}=${SAMPLE_PART_LINK}   ${content}=${SAMPLE_PART_CONTENT}
    wait until element is visible  ${PART_SEARCH_BOX}
    Run Keyword until Success    click element  ${PART_SEARCH_BOX}
    Run Keyword until Success    clear element text  ${PART_SEARCH_BOX}
    Run Keyword until Success    input text  ${PART_SEARCH_BOX}  ${text}
    wait until element is visible  ${part}
    Run Keyword until Success    click element  ${part}
    wait until element is visible  ${content}
clickSignOut
    [Documentation]  "Click 'sign out' option that appears from clickProfile"
    wait until element is visible  ${SIGNOUT_BTN}
    Run Keyword until Success    click element  ${SIGNOUT_BTN}