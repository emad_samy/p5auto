*** Settings ***
Library  SeleniumLibrary
Variables     ../../Resources/CommonKeywords/CommonVariables.py
Variables     ../../Resources/CommonKeywords/CommonLocators.py
Variables     ../../Resources/CommonKeywords/CommonAssertion.py
Variables     ../../Resources/ProjectManagementPages/ProjectBrowseAssertion.py
Variables     ../../Resources/ProjectManagementPages/ProjectBrowseLocators.py
*** Variables ***
&{Menu_Used}=     QA=${ACTION_MENU_QA}   PROD=${ACTION_MENU}
*** Keywords ***
verify project browse page
    wait until element is visible    ${PB_HEADING}
    element should contain    ${PB_HEADING}    ${PB_HEADER_CONTENT}
click bom
    wait until element is visible    ${bom}
    Run Keyword until Success    click element    ${bom}
click plus
    wait until element is visible    ${PLUS_BTN}
    Run Keyword until Success    click element    ${PLUS_BTN}
click upload new Bom
    wait until element is visible    ${UPLOAD_NEW}
    Run Keyword until Success    click element    ${UPLOAD_NEW}
progress through project browse
    verify project browse page
    click plus
    ProjectBrowseKeyword.click upload new Bom
click action menu
    [Arguments]    ${menu}=${Menu_Used.${ENVIRONMENT}}
    wait until element is visible    ${menu}
    Run Keyword until Success    click element    ${menu}




