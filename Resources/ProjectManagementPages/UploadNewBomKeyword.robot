*** Settings ***
Library  SeleniumLibrary
Variables     ../../Resources/ProjectManagementPages/UploadNewBomAssertion.py
Variables     ../../Resources/ProjectManagementPages/UploadNewBomLocators.py

*** Variables ***
*** Keywords ***
verify UNB tab
    wait until element is visible    ${UNB_HEADING}
    element should contain    ${UNB_HEADING}    ${UNB_HEADER_TEXT}
close UNB
    wait until element is visible    ${UNB_CLOSE}
    Run Keyword until Success    click element    ${UNB_CLOSE}
click select file
    [Arguments]    ${file}= ${FILE_TO_UPLOAD}
    wait until element is visible    ${UNB_SELECT_FILE}
    choose file    ${UNB_FILE}    ${file}
enter bom name
    [Arguments]    ${name_of_bom}= TEST BOM
    wait until element is visible    ${UNB_NAME_BOX}
    Run Keyword until Success    click element    ${UNB_NAME_BOX}
    Run Keyword until Success    input text    ${UNB_NAME_BOX}    ${name_of_bom}
click next
    wait until element is visible    ${UNB_NEXT}
    Run Keyword until Success    click element    ${UNB_NEXT}
progress through UNB
    verify UNB tab
    enter bom name
    click select file
    click next




