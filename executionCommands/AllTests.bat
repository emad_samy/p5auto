cd..
robot -N GeoriskTests -d Results\geoTestResults TestCases\SupplyChainTests\GeoriskApp.robot
robot -N MarketForecastTests -d Results\marketTestResults TestCases\SupplyChainTests\MarketForecastApp.robot
robot -N STSTests -d Results\stsTestResults TestCases\SupplyChainTests\SubTierSupplyApp.robot
robot -N TechProfTests -d Results\techTestResults TestCases\SupplyChainTests\TechProfileApp.robot
robot -N ConstantsTests -d Results\ConstantTestResults TestCases\SharedPagesTests\WebsiteConstantsApp.Robot
robot -N BannerTests -d Results\bannerTestResults TestCases\SharedPagesTests\BannerSearchApp.robot
robot -N SidebarTest -d Results\SidebarTestResults TestCases\SharedPagesTests\SidebarSearchApp.robot
robot -N FooterTest -d Results\FooterTestResults TestCases\SharedPagesTests\FooterTestApp.robot
robot -N EndToEndSupplyTests -d Results\EndToEndResults TestCases\EndToEndTest\SupplyChainEndToEnd.robot
robot -N ProjectBrowseTests -d Results\ProjectBrowseResults TestCases\ProjectBrowseTests\ProjectBrowseApp.robot



