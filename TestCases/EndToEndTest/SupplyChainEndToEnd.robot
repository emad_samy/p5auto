*** Settings ***
Library    String
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Resource    ../../Resources/DataManager.robot
Resource  ../../Resources/CommonKeywords/CommonKeyword.robot
Resource    ../../Resources/PartDetailPage/SpecificPartKeyword.robot
Resource    ../../Resources/PLNamePage/PLNameKeyword.robot
Resource    ../../Resources/ManufacturerPage/ManufacturerKeyword.robot
Resource  ../../Resources/SharedPages/BannerKeyword.robot
Resource    ../../Resources/SharedPages/SidebarKeyword.robot
Resource    ../../Resources/BomManagementPages/ActionMenuKeyword.robot
Resource    ../../Resources/BomManagementPages/BomMappingKeyword.robot
Resource    ../../Resources/BomManagementPages/BomMatchKeyword.robot
Resource    ../../Resources/BomManagementPages/BomMatchNewKeyword.robot
Resource    ../../Resources/BomManagementPages/MyBomsKeyword.robot
Resource    ../../Resources/ProjectManagementPages/ProjectBrowseKeyword.robot
Resource    ../../Resources/ProjectManagementPages/UploadNewBomKeyword.robot
Resource  ../../Resources/SupplyChainPages/GeoriskKeyword.robot
Resource  ../../Resources/SupplyChainPages/MarketForecastKeyword.robot
Resource    ../../Resources/SupplyChainPages/PartsListKeyword.robot
Resource  ../../Resources/SupplyChainPages/SubTierSupplyKeyword.robot
Resource  ../../Resources/SupplyChainPages/TechProfileKeyword.robot
Test Setup  access silicon expert
Test Teardown  logout action
*** Variables ***

*** Test Cases ***
End To End Supply Chain
    [Documentation]     "login, go to project, upload new bom, go to supply chain, each sc tab, go to project, delete Bom, logout"
    click my boms
    #click automation project
    progress through my boms
    #verify project browse
    #click plus button
    progress through project browse
    #click upload new Bom
    #Name Bom
    #Click select file
    progress through UNB
    #Veryify MPN and Manufacturer
    #Dropdown choose CPN
    #Dropdow2 choose Manufacturer
    #CLick continue
    progress through bom mapping
    #Verify Bom match 1
    #Click Finish
    progress through bom match
    #VErify Bom Match 2
    progress through new bom match
    #Cllick supply chain
    clickSupplyChain
    pause
    #click geo
    #geo actions
    select bom sequence
    enterEvents
    edit risk sequence
    show part sequence
    #market forecast
    market search sequence
    logout action
    access silicon expert
    clickSupplyChain
    clickMarketTab
    useFeatures
    apply filter sequence
    reset filter sequence
    #sts tab
    logout action
    access silicon expert
    clickSupplyChain
    clickSTSTab
    stsSearch
    logout action
    access silicon expert
    clickSupplyChain
    clickSTSTab
    stsFeatures
    #tech tab
    logout action
    access silicon expert
    clickSupplyChain
    clickTechProfTab
    techFeatures
    #delete
    click my boms
    click project
    click action menu
    deletion sequence




*** Keywords ***
