*** Settings ***
Library    String
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Resource    ../../Resources/DataManager.robot
Resource  ../../Resources/CommonKeywords/CommonKeyword.robot
Resource  ../../Resources/SharedPages/BannerKeyword.robot
Resource    ../../Resources/SharedPages/SidebarKeyword.robot
Resource    ../../Resources/BomManagementPages/ActionMenuKeyword.robot
Resource    ../../Resources/BomManagementPages/BomMappingKeyword.robot
Resource    ../../Resources/BomManagementPages/BomMatchKeyword.robot
Resource    ../../Resources/BomManagementPages/BomMatchNewKeyword.robot
Resource    ../../Resources/BomManagementPages/MyBomsKeyword.robot
Resource    ../../Resources/ProjectManagementPages/ProjectBrowseKeyword.robot
Resource    ../../Resources/ProjectManagementPages/UploadNewBomKeyword.robot
Resource  ../../Resources/SupplyChainPages/GeoriskKeyword.robot
Test Setup  access silicon expert
Test Teardown  logout action
*** Variables ***

*** Test Cases ***
delete bom test
    click my boms
    #click automation project
    progress through my boms
    #verify project browse
    #click plus button
    progress through project browse
    #click upload new Bom
    #Name Bom
    #Click select file
    progress through UNB
    #Veryify MPN and Manufacturer
    #Dropdown choose CPN
    #Dropdow2 choose Manufacturer
    #CLick continue
    progress through bom mapping
    #Verify Bom match 1
    #Click Finish
    progress through bom match
    #VErify Bom Match 2
    progress through new bom match
    #Cllick supply chain
    clickSupplyChain
    pause
    click my boms
    click project
    click action menu
    deletion sequence
