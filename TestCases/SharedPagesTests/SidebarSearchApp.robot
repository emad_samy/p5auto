*** Settings ***
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Resource  ../../Resources/SharedPages/BannerKeyword.robot
Resource  ../../Resources/SharedPages/SidebarKeyword.robot
Resource  ../../Resources/CommonKeywords/CommonKeyword.robot
Test Setup  access silicon expert
Test Teardown  logout action
*** Variables ***
@{Sample Texts}=  try 1    ${Sample_BOM_Text2}    ${Sample_BOM_Text}
*** Test Cases ***
part search
    search sidebar

*** Keywords ***
search sidebar
    [Documentation]    "Using an array, iterate through each term by searching for it in the sidebar BOM search box."
    FOR  ${text}  IN  @{Sample Texts}
        BOM Search  ${text}
        comment   ${text}
        log to console   ${text}    "string 2"
        log many    ${text}    "OnlyLogTest"
    END
