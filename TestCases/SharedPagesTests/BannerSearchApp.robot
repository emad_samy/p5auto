*** Settings ***
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Resource  ../../Resources/SharedPages/BannerKeyword.robot
Resource  ../../Resources/CommonKeywords/CommonKeyword.robot
Resource    ../../Resources/SharedPages/SidebarKeyword.robot
Library    DataDriver    file=../../TestData/ConstantSearch.xlsx  sheet_name=BannerSearch
Test Setup  access silicon expert
Test Teardown  logout action
Test Template  banner searcher
*** Variables ***

*** Test Cases ***
part search using ${SearchText}

*** Keywords ***
banner searcher
    [Documentation]    "Utilizes the banner search bar."
    [Arguments]  ${SearchText}
    pause
    search for part  ${SearchText}