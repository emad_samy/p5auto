*** Settings ***
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Resource  ../../Resources/SharedPages/BannerKeyword.robot
Resource  ../../Resources/SupplyChainPages/GeoriskKeyword.robot
Resource  ../../Resources/CommonKeywords/CommonKeyword.robot
Resource    ../../Resources/SharedPages/SidebarKeyword.robot
Resource    ../../Resources/DataManager.robot
Test Setup  access silicon expert
Test Teardown  logout action
*** Variables ***
@{type_list}
@{name_list}

*** Test Cases ***
#three comment tags, unit for single function tests, integration for elements that interact with other functions or outside resources, scenario test- test end to end cases, whole user experience from login to logout with relevant actions in between
#first run all unit tests. alllows for notifacation of isolated function failure. If these pass, run integration tests, to ensure interactions are functionable. Then finally the scenario tests are executed if the previous two tags were succesful
#When doing scenario tests initilize a new user, to login and execute paralell tests. During teardown delete new account. (reset state)
MapTabTest
    [Documentation]  "Toggles through all map tabs"
    [Tags]    "Integration"
    clickSupplyChain
    clickGeoriskTab
    geo click select bom
    pause
    geo choose BOM
    geo click select bom
    geo close choose bom
    clickWaferTest
    clickFabricationSite
    clickAssemblySite
    clickFinaltestSite
    clickCountryOfOrigin
Click Supply Test
    [Tags]    Unit
    clickSupplyChain
EventTest
    [Tags]    "Integration"
    [Documentation]  "Iterates through clickable events on georisk tab"
    clickSupplyChain
    enterEvents
EditRiskTest
    [Tags]    "Integration"

    clickSupplyChain
    click editable risk
    FOR    ${site}    IN    @{sites}
        enter percent    ${site}    ${EXAMPLE_PERCENT}
    END
    save percents
    close editable risk
    click editable risk
    FOR    ${site2}    IN    @{sites2}
        verify percents    ${site2}    ${EXAMPLE_PERCENT}
    END
    close editable risk
    click editable risk
    reset percents
    save percents
    close editable risk
    logout action
    access silicon expert
    clickSupplyChain
    pause
    pause
    pause
    click editable risk
    verify starter percents
    close editable risk
ResetFilterTest
    [Tags]    "Integration"
    clickSupplyChain
    loadingCompensation
    clickEventType
    selectType
    clickEventName
    selectName
    click reset filter
    pause
    clickEventType
    selectType
    clickEventName
    selectName
ShowPartsTest
    [Tags]    "Integration"
    clickSupplyChain
    click show parts
    search part
    click view
    click part number
    confirm part number
    click manufacturer
    confirm manufacturer
    click pl name
    confirm pl name
    back from map
SelectBOMTest
    clickSupplyChain
    geo click select bom
    pause
    geo click select bom search
    pause
    geo enter bom to search
    geo reset select bom
    geo choose BOM
    geo click select bom
    geo close choose bom
DropdownTest
    [Documentation]    "iterates through drop down menu options"
    [Tags]    "Unit"
    ${csv_data}=    get csv data    ${GEO_CSV_FILE}
    FOR    ${index}     IN RANGE    17
        ${placeholder}    set variable     ${csv_data}[${index}]
        ${type_list}    set variable    ${placeholder}[0]
        ${type_selected}     get variable value    ${type_list}
        ${name_list}     set variable    ${placeholder}[1]
        ${name_selected}    get variable value    ${name_list}
        run keyword and continue on failure    dropdown sequence    ${type_selected}     ${name_selected}
    END

*** Keywords ***

