*** Settings ***
Library    String
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Resource  ../../Resources/SharedPages/BannerKeyword.robot
Resource  ../../Resources/SupplyChainPages/MarketForecastKeyword.robot
Resource  ../../Resources/CommonKeywords/CommonKeyword.robot
Resource    ../../Resources/PartDetailPage/SpecificPartKeyword.robot
Resource    ../../Resources/PLNamePage/PLNameKeyword.robot
Resource    ../../Resources/ManufacturerPage/ManufacturerKeyword.robot
Resource    ../../Resources/SupplyChainPages/PartsListKeyword.robot
Resource    ../../Resources/SharedPages/SidebarKeyword.robot
Resource    ../../Resources/DataManager.robot
Test Setup  access silicon expert
Test Teardown  logout action
*** Variables ***

*** Test Cases ***
MarketSearchTest
    market search sequence
MarketFeatureTest
    [Documentation]  "Comprehensive usage of page's actions"
    clickSupplyChain
    pause
    clickMarketTab
    useFeatures
SelectBOMTest
    clickSupplyChain
    pause
    clickMarketTab
    click select bom
    click select bom search
    enter bom to search
    reset select bom
    choose BOM
    click select bom
    close choose bom
ApplyFilterTest
    clickSupplyChain
    pause
    apply filter sequence
ResetFilterTest
    clickSupplyChain
    pause
    reset filter sequence

*** Keywords ***
