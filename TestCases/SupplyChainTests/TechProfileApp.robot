*** Settings ***
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Resource  ../../Resources/SharedPages/BannerKeyword.robot
Resource  ../../Resources/SupplyChainPages/TechProfileKeyword.robot
Resource  ../../Resources/CommonKeywords/CommonKeyword.robot
Resource    ../../Resources/SharedPages/SidebarKeyword.robot
Suite Setup  access silicon expert
Suite Teardown  logout action

*** Variables ***

*** Test Cases ***
TechProfTabTest
    [Documentation]  "Comprehensive execution of all actionable events on tab"
    clickSupplyChain
    pause
    clickTechProfTab
    techFeatures
SelectBOMTest
    clickSupplyChain
    pause
    clickTechProfTab
    click select bom
    click select bom search
    enter bom to search
    reset select bom
    choose BOM
    click select bom
    close choose bom
*** Keywords ***

