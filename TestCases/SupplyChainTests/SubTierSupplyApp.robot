*** Settings ***
Resource    ../../Resources/LoginPage/LoginKeyword.robot
Resource  ../../Resources/SharedPages/BannerKeyword.robot
Resource  ../../Resources/SupplyChainPages/SubTierSupplyKeyword.robot
Resource  ../../Resources/CommonKeywords/CommonKeyword.robot
Resource    ../../Resources/SharedPages/SidebarKeyword.robot
Test Setup  access silicon expert
Test Teardown  logout action
*** Variables ***

*** Test Cases ***
SubTierSearchTest
    [Documentation]  "Executes a search on the Sub Tier Supply tab"
    clickSupplyChain
    pause
    clickSTSTab
    stsSearch
SubTierFeaturesTest
    [Documentation]  "Comprehensive execution of actionable items on the STS tab"
    clickSupplyChain
    pause
    clickSTSTab
    stsFeatures
    pause
SelectBOMTest
    clickSupplyChain
    pause
    clickSTSTab
    click select bom
    click select bom search
    enter bom to search
    reset select bom
    choose BOM
    click select bom
    close choose bom
*** Keywords ***
